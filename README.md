# IDS721 Final Project Group 12

[![pipeline status](https://gitlab.com/dukeaiml/IDS721/final_project_llmops/badges/main/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/final_project_llmops/-/commits/main)

## Project Overview 
In this project, we will focus on the practical application of machine learning by operationalizing an open-source LLM model. The core task is to serve the model with a **text summarization** goal through a robust web service developed using the [Rust](https://www.rust-lang.org/) programming language. The project encompasses several key aspects of modern data engineering workflow, including inference endpoints, [Docker](https://www.docker.com/) containerization, deployment & configurations to AWS-managed Kubernetes Service ([AWS EKS](https://aws.amazon.com/eks/) | [AWS EC2](https://aws.amazon.com/ec2/) | [AWS ECR](https://aws.amazon.com/ecr/)), automated deployment via CI/CD pipelines, effective service monitoring, and detailed documentation. A successful demonstration of the application via a YouTube video is also provided to showcase the functionality and utility of the service. 

## Demo Video

> Refer to the [last section](https://gitlab.com/dukeaiml/IDS721/final_project_llmops#demo-images) for our demo images!

Click below and get redirected to our YouTube demo video: 

[![Project Demo](http://img.youtube.com/vi/TpzC9H0FbRs/0.jpg)](http://www.youtube.com/watch?v=TpzC9H0FbRs)

## 1⃣️ Getting Started 

Before we start sailing, ensure the following tools have been installed and configured:

- **Rust**: A system programming language known for its memory safety and high performance. It is ideal for system-level tooling, application development, and especially useful in scenarios demanding high concurrency and safety. Ensure Rust is installed for building the web service. 👉[Install](https://www.rust-lang.org/tools/install)

- **Rust-Bert** *(crate)*: A Rust library for comprehensive NLP pipelines. This library will be used for handling the machine learning model. 📖 [Documentation](https://github.com/guillaume-be/rust-bert)
   - Depending on system architectures, [**tch**](https://docs.rs/tch/latest/tch/) *(crate)* and a prior installation of [**libtorch v2.2**](https://pytorch.org/get-started/locally/) might be required.
   - With Rust-Bert, refer to the [list of supported models](https://huggingface.co/models?library=rust&sort=trending) on **HuggingFace**, a repository of machine learning models and datasets. Make sure to sign up for an account. 
   - Based on our objective of text summarization, we chose the `facebook/bart-large-xsum` model with high performance in acceptable size. 👉[Install](https://huggingface.co/facebook/bart-large-xsum/blob/main/rust_model.ot)

- **Actix** *(crate)*: A Rust framework for building high-performance web services, used for developing the web application. 📖 [Documentation](https://crates.io/crates/actix)

- **Docker**: A platform-as-a-service product that facilitates the packaging and distribution of applications in lightweight containers. Docker containers encapsulate the application and its environment, ensuring consistency across multiple development and release cycles. 👉[Install](https://www.docker.com/products/docker-desktop/)

- **Kubernetes**: An open-source system for automating deployment, scaling, and management of containerized applications. It helps with managing containerized applications in different deployment environments and is crucial for orchestrating Docker containers. To use AWS hosted Kubernetes services, make sure you have an AWS account and [`aws cli`](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) for configurations:
   - **AWS Elastic Container Registry (ECR)**: A Docker container registry that simplifies the management and deployment of Docker images. Necessary for storing and managing the Docker images created.
   - **AWS Elastic Kubernetes Service (EKS)**: A managed Kubernetes service for easy deployment and management of serverless functions in the AWS cloud. This will be used for deploying the containerized service. Some additional tools are required to implement EKS:
      - [`eksctl`](https://eksctl.io/installation/):  A command line tool for working with EKS clusters that automates many individual tasks.
      - [`kubectl`](https://docs.aws.amazon.com/eks/latest/userguide/install-kubectl.html): A command line tool for working with Kubernetes clusters.
   - **AWS Elastic Compute Cloud (EC2)**: EC2 instances serve as the worker nodes on which Kubernetes pods and workloads are deployed and executed. 

- **Git**: A distributed version control system to track changes in source code during software development. It's essential for managing project files and collaborating on code development, especially when integrated with CI/CD pipelines for automated processes.
   - **Git LFS**: To provide a snapshot of the model of large size and access efficient storage on Git. 👉[Install](https://git-lfs.com/)

## 2⃣️ Building

### File Structure

```bash
├── EKS                     # Kubernetes config files
│   ├── eks-sample-deployment.yaml
│   └── eks-sample-service.yaml
├── final_llmops            # Rust Cargo project directory
│   ├── Cargo.toml          ## Crate dependencies
│   ├── Dockerfile          ## Docker containerization
│   └── src                 ## Rust implementation of LLM model
│       ├── handlers.rs
│       ├── main.rs
│       └── models.rs
└── rust_model.ot           # Example model for users easy download
└── .gitlab-ci.yml          # GitLab CI/CD pipeline
```

### Step 1: Grab Reproducible Source Code

1. **Clone the Repository**
   ```shell
   git clone https://gitlab.com/dukeaiml/IDS721/final_project_llmops.git
   cd final_project_llmops
   ```
2. **Navigate to the Source Code Directory**
   ```shell
   cd final_llmops
   ```
3. **Direct Execution**
   This command starts the application locally on port 8080:
   ```shell
   cargo run
   ```

### Step 2: Docker Containerization

1. **Create a Dockerfile**
   The Dockerfile should be in the project root resolving Docker image and container creation

2. **Build the Docker Image**
   Navigate to the project root and execute:
   ```shell
   docker build -t final_llmpos .
   ```
3. **Run the Container**
   This command runs the container and exposes it on local port 8080:
   ```shell
   docker run -p 8080:8080 -v <MODEL_PATH> final_llmpos
   ```

### Step 3: AWS Deployment w. Kubernetes

Deployment on AWS involves using AWS EKR for Docker image storage and AWS EKS for serverless function deployment (with AWS EC2 as compute instances):

1. **AWS ECR Setup**
   Create an AWS ECR repository for storing and managing the Docker image
2. **Docker Image Handling**
   Configure the containerized project with AWS and push the Docker image to our repo:
   ```shell
   aws ecr get-login-password --region <YOUR_REGION> | docker login --username AWS --password-stdin <REPO_ID>.dkr.ecr.us-east-1.amazonaws.com
   docker tag final_llmpos:latest <REPO_ID>.dkr.ecr.us-east-1.amazonaws.com/final_llmpos:latest
   docker push <REPO_ID>.dkr.ecr.us-east-1.amazonaws.com/final_llmpos:latest
   ```
3. **Cluster Deployment on AWS EKS**
   Use `eksctl` to create the Kubernetes cluster and manage via `kubectl`:
   ```shell
   eksctl create cluster --name <CLUSTER_NAME> --region <YOUR_REGION>
   cd EKS
   kubectl create namespace <APP_NAMESPACE>
   kubectl apply -f eks-sample-deployment.yaml
   kubectl apply -f eks-sample-service.yaml  
   ```
   The two `.yaml` files specify detailed Kubernetes cluster configurations and settings, such as pods and nodes infomation. The scalability and reliability are also defined here following our desired state of deployment. 

4. **Enable Logging & Monitoring via CloudWatch**
   In order to peek into the core vitals of our service, AWS CloudWatch provides convenient metrics tracing and traffic analyses with easy implementation on AWS EKS: 
   ```shell
   eksctl utils update-cluster-logging --enable-types all --cluster <CLUSTER_NAME> --region <YOUR_REGION> --approve
   ```
   *Note: Don't forget to attach relavant policies to your IAMs & EKS roles!*

### Step 4: CI/CD Pipeline

CI/CD pipeline has been effectively incorporated to automate the build-test-deploy stages of our project. We have carefully configured through all stages to ensure the pipeline succeed. Refer to the [.gitlab-ci.yml](https://gitlab.com/dukeaiml/IDS721/final_project_llmops/-/blob/main/.gitlab-ci.yml?ref_type=heads) file for details!

*Note: With CI/CD, we directly download the model from source and build from there. The [rust_model.ot](https://gitlab.com/dukeaiml/IDS721/final_project_llmops/-/blob/main/rust_model.ot?ref_type=heads) file is only used for our initial local development and for representation to our viewers.*


## 3⃣️ Application

Ensuring the build and deploy stage is working correctly, we can now post a JSON-structured text to the summarization endpoint:
```json
{
    "text": "The tower is 324 metres (1,063 ft) tall, about the same height as an 81-storey building, and the tallest structure in Paris. Its base is square, measuring 125 metres (410 ft) on each side. During its construction, the Eiffel Tower surpassed the Washington Monument to become the tallest man-made structure in the world, a title it held for 41 years until the Chrysler Building in New York City was finished in •1930. It was the first structure to reach a height of•300 metres. Due to the addition of a broadcasting aerial at the top of the tower in 1957, it is now taller than the Chrysler Building by 5.2 metres (17 ft). Excluding transmitters, the Eiffel Tower is the second tallest free-standing structure in France after the Millau Viaduct."
}
```
The above request will return:
```json
{
    "summary": "The Eiffel Tower in Paris has been officially opened to the public for the first time them"
}
```

## Demo Images

- **Local Test**

![model_inference_local_test](https://cdn.mathpix.com/snip/images/IS1L31yKD5S75Zz9yJ-tQVRXKyA7lraUb1kHcPLd8n4.original.fullsize.png)

- **Docker Image & Container**

![docker_images](https://cdn.mathpix.com/snip/images/wdJJuw9I7vTUrRvLPqJ4HgVRJD_UKxf7dDIW07iu0U8.original.fullsize.png)

![docker_containers](https://cdn.mathpix.com/snip/images/tUNgZAc1Am4aYWCZvATIpYdFpfkUXCk3Lg3PiMa5JOg.original.fullsize.png)

- **AWS ECR Repository**

![ECR](https://cdn.mathpix.com/snip/images/B4DBbj-J6qFzZVyxxbhy2vWWRYkUhou4wZk_n_r_vEk.original.fullsize.png)

- **AWS EKS Cluster w. Pods**

![EKS](https://cdn.mathpix.com/snip/images/27GZ0jTvmDHlhfy3PkhWLiKkobr9y-j0Nv5eCbiA0TY.original.fullsize.png)

![EKS_Pods](https://cdn.mathpix.com/snip/images/iQpyG1oCqY_Tbht1uL8RMeLbxBzuqd6aKA6VwCu0x48.original.fullsize.png)

- **AWS EC2 & Load Balancer**

![EC2](https://cdn.mathpix.com/snip/images/l5zNVAb7fa4nuIda9ndAjKMQ6jGZlOm01jDroKYX6Xo.original.fullsize.png)

![load_balancer_new](https://cdn.mathpix.com/snip/images/jsAM7DACmHADo1wvGXe1YmoV9jLYw4YlHJHIvkZZu0g.original.fullsize.png)

- **AWS CloudWatch Logging & CloudFormation Stack**

![Cloudwatch](https://cdn.mathpix.com/snip/images/TpLi0swojMN-yPToD4vOALApZ9LXjo3UtO7mYwz7y54.original.fullsize.png)

![Cloud_formation](https://cdn.mathpix.com/snip/images/1NwNv8lBPT2no0fJp0poi9XD6OPOCXt-lnFpgp64Ccc.original.fullsize.png)

- **Load Testing**

[Postman](https://www.postman.com/) was used for load testing our web service to simulate various traffic scenarios and evaluate the service's performance under load. It helps us identify bottlenecks and assess the web service's capacity to handle increasing traffic or concurrent users:

![postman_for_demo_new](https://cdn.mathpix.com/snip/images/gYxVjgekFZtn4BJi0DRcvL66KwRfqwkD-OtSvIJAsxE.original.fullsize.png)


