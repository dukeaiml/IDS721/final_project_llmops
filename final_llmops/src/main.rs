use actix_web::{web, App, HttpServer};
use rust_bert::pipelines::common::ModelResource;
use rust_bert::pipelines::summarization::{SummarizationModel, SummarizationConfig};
use rust_bert::resources::{LocalResource};
use std::sync::Mutex;
use std::path::PathBuf;

mod handlers;
mod models;
use models::SharedData;
use handlers::summarize_text;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    std::env::set_var("RUST_LOG", "debug");
    env_logger::init();

    // Define local resources for the model components
    let model_resource = Box::new(LocalResource {
        local_path: PathBuf::from("/usr/local/share/final_llmops/model/rust_model.ot"),
    });

    let config_resource = Box::new(LocalResource {
        local_path: PathBuf::from("/usr/local/share/final_llmops/model/config.json"),
    });

    let vocab_resource = Box::new(LocalResource {
        local_path: PathBuf::from("/usr/local/share/final_llmops/model/vocab.json"),
    });

    let merges_resource = Box::new(LocalResource {
        local_path: PathBuf::from("/usr/local/share/final_llmops/model/merges.txt"),
    });

    // Configuration for the summarization model
    let summarization_model = SummarizationModel::new(SummarizationConfig {
        model_resource: ModelResource::Torch(model_resource),
        config_resource: config_resource,
        vocab_resource: vocab_resource,
        merges_resource: Some(merges_resource),
        max_length: Some(20),  // Adjust the max_length as needed
        length_penalty: 2.0,
        num_beams: 100,    // Set number of beams for beam search
        ..Default::default()
    });

    let summarization_model = match summarization_model {
        Ok(model) => model,
        Err(e) => {
            eprintln!("Failed to create summarization model: {:?}", e);
            return Err(std::io::Error::new(std::io::ErrorKind::Other, "Model initialization failed"));
        }
    };

    let model_data = web::Data::new(Mutex::new(SharedData { model: summarization_model }));

    HttpServer::new(move || {
        App::new()
            .app_data(model_data.clone())
            .route("/summarize", web::post().to(summarize_text))
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}