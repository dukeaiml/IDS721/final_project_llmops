use crate::models::{Summary, TextQuery, SharedData};
use actix_web::{web, HttpResponse, Responder};
use std::sync::Mutex;

pub async fn summarize_text(
    data: web::Data<Mutex<SharedData>>,
    query: web::Json<TextQuery>
) -> impl Responder {
    let model_lock = data.lock().unwrap();
    let model = &model_lock.model;

    let result = model.summarize(&[query.text.as_str()]);

    match result {
        Ok(summaries) => {
            let summary = summaries.join(" "); // Assuming there might be multiple sentences returned
            HttpResponse::Ok().json(Summary { summary })
        },
        Err(_) => HttpResponse::InternalServerError().json("Error during summarization")
    }
}


