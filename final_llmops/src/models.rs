use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
pub struct TextQuery {
    pub text: String,
}

#[derive(Serialize)]
pub struct Summary {
    pub summary: String,
}

pub struct SharedData {
    pub model: rust_bert::pipelines::summarization::SummarizationModel,
}

